import pandas as pd
import numpy as np
import os
import shutil
import requests
import zipfile
import io
import math
from operator import add
from datetime import datetime, timedelta
from fuzzywuzzy import fuzz



def get_DWD_data(weather_aspect, url):

    folder = "DWD_" + weather_aspect
    try:
        shutil.rmtree(folder)
    except OSError as e:
        print(f"Error: {e}")

    r = requests.get(url)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall(folder)
    product_file = None
    for file in os.listdir(folder):
        if "produkt" in file:
            product_file = folder + "/" + file
            break
    product_data = pd.read_csv(product_file, sep=";")
    return product_data



def prepare_data(raw_data, column, year):

    start_date = int(str(year) + "010100")
    end_date = int(str(year) + "123123")
    start = datetime(year, 1, 1, 0)
    end = datetime(year, 12, 31, 23)
    all_possible_dates = [int((start+timedelta(hours=i)).strftime("%Y%m%d%H"))
                          for i in range(int((end - start).total_seconds()
                                             / 3600) + 1)]
    data_sum = np.array([0]*len(all_possible_dates)).astype(np.float64)
    for data in raw_data:
        # filter for year and interpolate errors
        selected_data = data.loc[(start_date <= data["MESS_DATUM"])
                                 & (data["MESS_DATUM"] <= end_date)]
        selected_data = selected_data.replace({-999.0: np.nan})
        selected_data = selected_data.interpolate()

        # remove duplicates
        selected_data = selected_data.drop_duplicates(subset="MESS_DATUM")
        # fill missing dates
        selected_data = selected_data.set_index("MESS_DATUM")
        selected_data = selected_data.reindex(all_possible_dates, method="nearest")
        data_sum += np.array(selected_data[column]).astype(np.float64)

    average_data = data_sum/len(raw_data)
    return average_data


def pv_yield_calculation(nominal_power, BHI, DHI, GHI, temperature, latitude=51.8080, longitude=10.3407, #Längen- und Breitengrad Clausthal-Zellerfeld
                         temperature_coefficient = -0.48, orientation="Süd", inclination=30, mounting_type = "Bauliche Anlagen (Hausdach, Gebäude und Fassade)"):
    # defining constants:
    solar_constant = 1356.5
    albedo = 0.2
    derating_factor = 0.85
    nominal_power_plant = nominal_power
    tilt_angle_modul = inclination

    # Liste von 0 - 23 für 24 Stunden eines Tages erstellen,
    # 365 mal multiplizieren um ganzes Jahr zu erhalten, 
    # mal 60 plus 30 Minuten von 00:00 bis zur Stundenmitte zu erhalten
    TLT_hour = list(range(0, 24)) * 365
    time_min = [time*60+30 for time in TLT_hour]

    # Look-up-Tabelle zur Zuordnung der Ausrichtung zur Gradzahl
    orientation_table = {
        "Nord": 0,
        "Nord-Ost": 45,
        "Ost": 90,
        "Süd-Ost": 135,
        "Süd": 180,
        "Süd-West": 225,
        "West": 270,
        "Nord-West": 315,
        "Ost-West": 180,
        "nachgeführt": 180
    }

    # Nachschlagetabelle zur Zuordnung von mounting_type zu delta_t
    delta_t_table = {
        "Bauliche Anlagen (Hausdach, Gebäude und Fassade)": 29,
        "Bauliche Anlagen (Sonstige)": 30,
        "Steckerfertige Solaranlage (sog. Balkonkraftwerk)": 40,
        "Freifläche": 22
    }

    orientation_angle_module = orientation_table[orientation]

    day = [x for x in range(1, 366) for _ in range(24)]
    J = [360*d/365 for d in day]

    TEQ_min = [(0.0066 + 7.3525 * math.cos(math.radians(j+85.9))) + 9.9359 * math.cos(math.radians(2*j+108.9)) + 0.3387*math.cos(math.radians(3*j+105.2)) for j in J]    
    declination = [ math.radians(0.3948 -23.2559 * math.cos(math.radians(j+9.1)) -0.3915 * math.cos(math.radians(2*j+5.4)) -0.1764 * math.cos(math.radians(3*j+26))) for j in J]

    # MLT_adj: Abkürzung für "Modified Local Time Adjustmen; TLT_adj: Abkürzung für "Total Local Time Adjustment"; TLT_min: Eine leere Liste, die dazu verwendet wird, die bereinigten lokalen Zeiten zu speichern.; hour_angle_radian: Eine Liste von Winkeln im Bogenmaß, die den Stundenwinkel für jede bereinigte lokale Zeit (TLT) repräsentieren.
    MLT_adj = [time + 4*longitude for time in time_min]
    TLT_adj = list(map(add, MLT_adj, TEQ_min))
    TLT_min = []
    for TLT in TLT_adj:
        if TLT > 0:
            TLT_min.append(TLT)
        else:
            TLT_min.append(24*60+TLT)

    hour_angle_radian = [math.radians(15*(12-TLT/60)) for TLT in TLT_min]

    # sun elevation 
    sun_elevation_angle_radian = []
    for hour in range(0, 8760):
        sun_elevation_angle_radian.append(math.asin(math.cos(hour_angle_radian[hour]) * math.cos(declination[hour]) * math.cos(math.radians(latitude)) + math.sin(math.radians(latitude))*math.sin(declination[hour])))
    sun_elevation_angle_degree = [math.degrees(elevation_radian) for elevation_radian in sun_elevation_angle_radian] 
    adjusted_solar_constant = [solar_constant + 48.5 * math.cos(0.01721 * (d-15)) for d in day]
    I_ex = [solar_const*math.sin(sun_elev) for solar_const, sun_elev in zip(adjusted_solar_constant, sun_elevation_angle_radian)]
    k_t = [0]*8760
    for i, k in enumerate(k_t):
        if GHI[i] > 0 and I_ex[i] > 0:
            k_t[i] = GHI[i]/I_ex[i]

    # BNI calculation
    BNI = [0]*8760
    for i, elevation_degree in enumerate(sun_elevation_angle_degree):
        if elevation_degree > 1:
            if sun_elevation_angle_radian[i] > 0:
                BNI[i] = BHI[i] / math.sin(sun_elevation_angle_radian[i])
            else:
                BNI = 0
        else:
            BNI[i] = BHI[i]

    r_hor_dif = DHI 
    r_hor_total = list(map(add, BNI, r_hor_dif))

    sun_azimuth = [0]*8760
    for i, hour in enumerate(TLT_hour):
        if hour <= 12:
            sun_azimuth[i] = math.pi - math.acos((math.sin(sun_elevation_angle_radian[i]) * math.sin(math.radians(latitude)) - math.sin(declination[i])) / (math.cos(sun_elevation_angle_radian[i]) * math.cos(math.radians(latitude))))
        else:
            sun_azimuth[i] = math.pi + math.acos((math.sin(sun_elevation_angle_radian[i]) * math.sin(math.radians(latitude)) - math.sin(declination[i])) / (math.cos(sun_elevation_angle_radian[i]) * math.cos(math.radians(latitude))))

    rad_angle_of_incidence = []
    for azimuth, elevation in zip(sun_azimuth, sun_elevation_angle_radian):
        rad_angle_of_incidence.append(math.acos(
              -1*math.cos(elevation) * math.sin(math.radians(tilt_angle_modul))
              * math.cos(azimuth-math.radians(orientation_angle_module)-math.pi) 
              + math.sin(elevation) * math.cos(math.radians(tilt_angle_modul))))

    # r_tilted calculation
    r_tilted_ref = [hor_total * albedo * 0.5 * (1 - math.cos(math.radians(tilt_angle_modul))) for hor_total in r_hor_total]
    r_tilted_dif = [hor_dif * 0.5 * (1 + math.cos(math.radians(tilt_angle_modul))) for hor_dif in r_hor_dif]
    r_tilted_dir = []
    for B, incidence in zip(BNI, rad_angle_of_incidence):
        r_tilted_dir.append(max(B * math.cos(incidence), 0))
    r_tilted_total = []
    for dir, dif, ref in zip(r_tilted_dir, r_tilted_dif, r_tilted_ref):
        r_tilted_total.append(dir + dif + ref)

    delta_t = delta_t_table[mounting_type]
    t_module = []
    for temp, tilted_total in zip(temperature, r_tilted_total):
        t_module.append(temp + delta_t * (tilted_total/1000))

    # putting it all together
    pv_yield = []

    for tilted_total, module in zip(r_tilted_total, t_module):
        pv_yield.append((derating_factor * nominal_power_plant * tilted_total * (1+((temperature_coefficient/100) * (module-25))))/1000)
    return pv_yield


def calculate_pv_yield(solar_data_global, solar_data_diffuse, temperature_data):
    """

    Parameters
    ----------
    solar_data_global : DataFrame
        DESCRIPTION.
    solar_data_diffuse : DataFrame
        DESCRIPTION.
    temperature_data : DataFrame
        DESCRIPTION.

    Returns
    -------
    pv_rate_df : TYPE
        DESCRIPTION.

    """
    # beam + diffuse radiation = global, -> beam = global - diffuse
    #Umrechnung J/cm^2 --> Wh/m^2 -->cor=corrected
    cor_solar_data_global = [2.7777 * i for i in solar_data_global]
    cor_solar_data_diffuse = [2.7777 * i for i in solar_data_diffuse]
    FB_LBERG = [glob - dif for glob, dif
                in zip(cor_solar_data_global, cor_solar_data_diffuse)]

    pv_rate = [0] * 8760

    energy_production = pv_yield_calculation(10, FB_LBERG, cor_solar_data_diffuse, cor_solar_data_global, temperature=temperature_data) # 10 = 10kWp installierte Leistung 
    pv_rate = list(map(add, pv_rate, energy_production))
    pv_rate_df = pd.DataFrame({"H1": pv_rate})
    return pv_rate_df


def get_data():
    selected_year = 2021
    # sources
    # solar: Braunschweig, Bremen, Trier-Petrisberg
        
    temperature_braunschweig  = get_DWD_data("temperature", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/air_temperature/historical/stundenwerte_TU_00662_19510101_20231231_hist.zip")
    temperature_bremen    = get_DWD_data("temperature", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/air_temperature/historical/stundenwerte_TU_00691_19490101_20231231_hist.zip")
    temperature_trier_petrisberg = get_DWD_data("temperature", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/air_temperature/historical/stundenwerte_TU_05100_19410101_20231231_hist.zip")
    temperature = prepare_data([temperature_braunschweig, temperature_bremen, temperature_trier_petrisberg], "TT_TU", selected_year)

    solar_braunschweig  = get_DWD_data("solar", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/solar/stundenwerte_ST_00662_row.zip")
    solar_bremen    = get_DWD_data("solar", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/solar/stundenwerte_ST_00691_row.zip")
    solar_trier_petrisberg = get_DWD_data("solar", "https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/hourly/solar/stundenwerte_ST_05100_row.zip")

    # solar data: removal of seconds 
    solar_braunschweig["MESS_DATUM"]  = [int(date[:-3]) for date in solar_braunschweig["MESS_DATUM"]]
    solar_bremen["MESS_DATUM"]    = [int(date[:-3]) for date in solar_bremen["MESS_DATUM"]]
    solar_trier_petrisberg["MESS_DATUM"] = [int(date[:-3]) for date in solar_trier_petrisberg["MESS_DATUM"]]
    solar_global = prepare_data([solar_braunschweig, solar_bremen, solar_trier_petrisberg], "FG_LBERG", selected_year)
    solar_diffuse = prepare_data([solar_braunschweig, solar_bremen, solar_trier_petrisberg], "FD_LBERG", selected_year)



    pv_source = calculate_pv_yield(solar_global, solar_diffuse, temperature)


    # V2G storage
    EV_amount = 1 # bei 2 Fahrzeugen logischerweise 2...
    EV_storage_capacity = 60
    # EV_storage_capacity_all = EV_amount * EV_storage_capacity


    # sinks
    H1_Baseload = pd.read_excel("Daten/H1_Baseload.xlsx")
    H1_Geschirr = pd.read_excel("Daten/H1_Geschirr.xlsx")
    H1_Waesche = pd.read_excel("Daten/H1_Waesche.xlsx")
    H1_Mobility= pd.read_excel("Daten/H1_Mobility.xlsx")
    
    mobility_demand = (([0]*12 + [10*EV_amount]*2 + [0]*4 + [10*EV_amount]*2 + [0]*4)*5 + [0]*19 + [5*EV_amount]*2 + [0]*21 + [5*EV_amount]*2 + [0]*4 )*52
    mobility_demand = mobility_demand + mobility_demand[0:24]
    mobility_sink = pd.DataFrame({"H1": mobility_demand})

    
    # electricity price
    electricity_price = pd.read_excel("Daten/Gro_handelspreise_202101010000_202112312359_Stunde.xlsx")
    electricity_price["Deutschland/Luxemburg [€/MWh]"] = pd.to_numeric(electricity_price["Deutschland/Luxemburg [€/MWh]"].astype(str).str.replace(',',    '.'), errors='coerce')
    electricity_market_price = pd.DataFrame({"H1": electricity_price["Deutschland/Luxemburg [€/MWh]"]})
    electricity_import_price = electricity_market_price.applymap(lambda x: (x/1000+0.1882+0.4888)) #Kein Aufschlag bei reinem Börsenpreis, Alle Umlagen bis auf Netzengelt +0.1143; Alle Umlagen, Steuern und Abgaben + 0.1965; Alle Umlagen, Steuern + Aufschlag von 0.0841 --> Preis ab dem die Verwendung des Basisszenarios im Jahr 2019 wirtschaftlicher ist; Alle Umlagen, Steuern und Abgaben + Aufschlag von 0.2564 --> Preis ab dem die Verwendung des Basisszenarios im Jahr 2021 wirtschaftlicher ist; Alle Umlagen + 0.1965, Steuern und Abgaben + Aufschlag von 0.4888 --> Preis ab dem die Verwendung des Basisszenarios im Jahr 2023 wirtschaftlicher ist,
    electricity_market_price = electricity_market_price.applymap(lambda x: x/1000)
    # Unterteilung in positive und negative Werte
    positive_prices = []
    negative_prices = []
    positive_rate_max = []
    negative_rate_max = []
    positive_prices_import = []
    negative_prices_import = []
    positive_rate_max_import = []
    negative_rate_max_import = []
    for price in electricity_market_price["H1"]:
        if price > 0:
            positive_prices.append(price)
            negative_prices.append(0)  # Bei positiven Werten 0 an die Negativliste anhängen
            positive_rate_max.append(1)
            negative_rate_max.append(0)
        else:
            positive_prices.append(0)  # 0 an die Positivliste für negative Werte anhängen
            negative_prices.append(price*-1)
            positive_rate_max.append(0)
            negative_rate_max.append(1)
    positive_prices = pd.DataFrame({"H1": positive_prices})
    negative_prices = pd.DataFrame({"H1": negative_prices})
    positive_rate_max = pd.DataFrame({"H1": positive_rate_max})
    negative_rate_max = pd.DataFrame({"H1": negative_rate_max})
    
    for price in electricity_import_price["H1"]:
        if price > 0:
            positive_prices_import.append(price)
            negative_prices_import.append(0)  # Bei positiven Werten 0 an die Negativliste anhängen
            positive_rate_max_import.append(1)
            negative_rate_max_import.append(0)
        else:
            positive_prices_import.append(0)  # 0 an die Positivliste für negative Werte anhängen
            negative_prices_import.append(price*-1)
            positive_rate_max_import.append(0)
            negative_rate_max_import.append(1)
    positive_prices_import = pd.DataFrame({"H1": positive_prices_import})
    negative_prices_import = pd.DataFrame({"H1": negative_prices_import})
    positive_rate_max_import = pd.DataFrame({"H1": positive_rate_max_import})
    negative_rate_max_import = pd.DataFrame({"H1": negative_rate_max_import})


    # V2G Profile
    profile = pd.read_excel("Daten/Connection_Times.xlsx")
    print(profile)

    profile = [profile] 
    

    data = {"pv_source": pv_source, 
            "EV_storage_capacity": EV_storage_capacity, 
            "H1_Baseload": H1_Baseload, 
            "H1_Geschirr": H1_Geschirr, 
            "H1_Waesche": H1_Waesche,
            "H1_Mobility": H1_Mobility, 
            "electricity_market_price": electricity_market_price,
            "positive_prices": positive_prices,
            "negative_prices": negative_prices,
            "positive_rate_max": positive_rate_max,
            "negative_rate_max": negative_rate_max,
            "positive_prices_import": positive_prices_import,
            "negative_prices_import": negative_prices_import,
            "positive_rate_max_import": positive_rate_max_import,
            "negative_rate_max_import": negative_rate_max_import}
            
    return data, profile