# Energiemanagement und dynamische Stromtarife – Eine Analyse für Einfamilienhäuser aus Nutzersicht unter Berücksichtigung von Vehicle- to-Grid-Aspekten Masterarbeit Flemming Stötzer

## Szenarien der betrachteten Jahr: 
H1 Basis

H1 intelligent charging

H1 only V2H

H1 only V2G 

H1 intelligent charging

## Tests für Bestandteile der Modellierung und deren Verhalten in unterschiedlichen Situationen 
H1 BEV statisch,
H1 BEV dynamische --> Test des BEV

H1 V2G --> Test von V2G

H1 Geschirrspüler Dynamisch,
H1 Geschirrspüler Statisch --> Test des Geschirrspülers bei dynamischem und statischem Strompreis

H1 Waschmaschine & Trockner Dynamisch,
H1 Waschmaschine & Trockner Statisch --> Test von Waschmaschine und Trockner bei dynamischem und statischem Strompreis

## GetData-Funktion für Szenarien 
getData_H1_[Jahr]_NP 

getData_H1_[Jahr]_NP_V --> getData der Wirtschaftlichkeitsbetrachtung hier als Preisvariation bezeichnet